# Environment for mobile manipulator
Basic OpenAI gym environment. 
The robot consists of a clearpath boxer and a franka emika panda.

## Installation
Install through
```bash
pip3 install -e /path/to/repo
```
