import gym
import numpy as np
import time
import pybullet as p
from pybullet_utils import bullet_client
from albert.resources.robot import Robot
from albert.resources.plane import Plane
from albert.resources.goal import Goal
from albert.resources.curveGoal import CurveGoal
from albert.resources.robotSpecs import RobotSpecs
import matplotlib.pyplot as plt


class AlbertEnv(gym.Env):
    metadata = {"render.modes": ["human"]}

    def __init__(self, render=False, discrete=False):
        self.robotSpec = RobotSpecs()
        self.observation_space = self.robotSpec.getObservationSpace()
        self.np_random, _ = gym.utils.seeding.np_random()
        self.robot = None
        self._isRender = render
        self.goal = None
        self.clientId = -1
        self.done = False
        self.prev_dist_to_goal = None
        self.rendered_img = None
        self.render_rot_matrix = None
        self._timeStep = 0.01
        self._numSubSteps= 20
        self._nSteps = 0
        self._maxSteps = 10000000
        self._p = p
        self._discrete = discrete
        if self._discrete:
            self.action_space = self.robotSpec.getDiscreteActionSpace()
        else:
            self.action_space = self.robotSpec.getActionSpace()
        if self._isRender:
            cid = p.connect(p.SHARED_MEMORY)
            if (cid < 0):
                cid = p.connect(p.GUI)
        else:
            p.connect(p.DIRECT)
        self.reset(initialSet=True)
        #self.initSim(timeStep=0.01, numSubSteps=20)

    def step(self, action):
        # Feed action to the robot and get observation of robot's state
        self._nSteps += 1
        if self._discrete:
            self.robot.apply_action(self.robotSpec.decodeAction(action))
        else:
            self.robot.apply_action(action)
        self._p.stepSimulation()
        goal_ob = self.goal.get_observation()
        robot_ob = self.robot.get_observation()
        ee_ob = robot_ob[-3:]

        # Compute reward as L2 change in distance to goal
        dist_to_goal, stepReward = self.goal.dist(ee_ob)
        #reward = max(self.prev_dist_to_goal - dist_to_goal, 0)
        self.prev_dist_to_goal = dist_to_goal

        # Done by running off boundaries
        (validReward, self.done, robotSpecInfo) = self.robotSpec.validateObservation(robot_ob)
        info = {"robotSpecInfo": robotSpecInfo}
        # Done by reaching goal
        # reward = -dist_to_goal + validReward + stepReward
        reward = validReward + 1.0
        if dist_to_goal < 0.1:
            self.done = True
            reward = 1

        ob = np.array(robot_ob + goal_ob, dtype=np.float32)
        if self._nSteps > self._maxSteps:
            reward = reward + 1
            self.done = True
        return ob, reward, self.done, info

    def seed(self, seed=None):
        self.np_random, seed = gym.utils.seeding.np_random(seed)
        return [seed]

    def initSim(self, timeStep, numSubSteps):
        if self.isRender:
            self._p = bullet_client.BulletClient(connection_mode=pybullet.GUI)
        else:
            self._p = bullet_client.BulletClient()
        self.clientId = self._p._client

        self._p.setPhysicsEngineParameter(
            fixedTimeStep=timeStep, numSubSteps=numSubSteps
        )
        self._p.setGravity(0, 0, -10)
        # Load the plane and robot
        self.plane = Plane(self.clientId)
        self.robot = Robot(self.clientId)
        self.goal = CurveGoal(self.clientId)
        #self.goal = Goal(self.clientId)
        self.done = False

        # Visual element of the goal
        self.initState = self._p.saveState()

    def reset(self, initialSet=False):
        if not initialSet:
            print("Run " + str(self._nSteps) + " steps in this run")
            self._nSteps = 0
            #self._p.restoreState(self.initState)
            p.resetSimulation()
        self._p.setPhysicsEngineParameter(
            fixedTimeStep=self._timeStep, numSubSteps=self._numSubSteps
        )
        self.plane = Plane()
        self.robot = Robot()
        self.goal = CurveGoal()
        self._p.setGravity(0, 0, -10)

        p.stepSimulation()

        # Get observation to return
        robot_ob = self.robot.get_observation()

        self.prev_dist_to_goal = self.goal.dist(robot_ob[:-3])
        return np.array(robot_ob + self.goal.get_observation(), dtype=np.float32)

    def render(self, mode="none"):
        return
        """
        if mode == "human":
            self.isRender = True
        else:
            self.isRender = False
        if self.initialRender:
            self.initialRender = False
            self.initSim(timeStep=0.02, numSubSteps=20)
        """

    def close(self):
        self._p.disconnect()
