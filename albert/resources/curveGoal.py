import pybullet as p
import os
import math
import time


class CurveGoal:
    def __init__(self):
        f_name = os.path.join(os.path.dirname(__file__), "simplegoal.urdf")
        self.ee_targets = [
            [0.656, 1.0, 1.12],
            [0.956, 1.0, 1.22],
            [1.256, 1.0, 1.32],
            [1.556, 1.0, 1.42],
        ]
        self.fut_targets = [self.ee_targets[0], self.ee_targets[1]]
        self.cur_last_target = 1
        for i in range(len(self.ee_targets)):
            target = self.ee_targets[i]
            p.loadURDF(
                fileName=f_name,
                basePosition=[target[0], target[1], target[2]]
            )

    def stepOnCurve(self):
        self.fut_targets.pop(0)
        self.cur_last_target = self.cur_last_target + 1
        if self.cur_last_target < len(self.ee_targets):
            self.fut_targets.append(self.ee_targets[self.cur_last_target])
        else:
            self.fut_targets.append(self.ee_targets[-1])

    def dist(self, ee_ob):
        d = self.singleDist(ee_ob)
        reward = 0
        if d < 0.1:
            reward = 100
            self.stepOnCurve()
        return (self.singleDist(ee_ob), reward)

    def singleDist(self, ee_ob):
        dist_to_goal = math.sqrt(
            (
                (self.fut_targets[0][0] - ee_ob[0]) ** 2
                + (self.fut_targets[0][1] - ee_ob[1]) ** 2
                + (self.fut_targets[0][2] - ee_ob[2]) ** 2
            )
        )
        return dist_to_goal

    def get_observation(self):
        flat_list = []
        for sublist in self.fut_targets:
            for item in sublist:
                flat_list.append(item)
        return tuple(flat_list)
