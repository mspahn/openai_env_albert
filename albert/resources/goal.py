import pybullet as p
import os
import math


class Goal:
    def __init__(self):
        f_name = os.path.join(os.path.dirname(__file__), 'simplegoal.urdf')
        self.ee_target = (1.0, 0.0, 1.0)
        p.loadURDF(fileName=f_name,
                   basePosition=[self.ee_target[0], self.ee_target[1], self.ee_target[2]])

    def dist(self, ee_ob):
        dist_to_goal = math.sqrt(
            ((self.ee_target[0] - ee_ob[0]) ** 2 + (self.ee_target[1] - ee_ob[1]) ** 2 + (self.ee_target[2] - ee_ob[2]) ** 2)
        )
        return dist_to_goal

    def get_observation(self):
        return self.ee_target


