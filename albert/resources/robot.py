import pybullet as p
import pybullet_data
import os
import math


class Robot:
    def __init__(self):
        f_name = os.path.join(os.path.dirname(__file__), 'albert_urdf/albert.urdf')
        self.robot = p.loadURDF(fileName=f_name,
                              basePosition=[0, 0, 0.1])

        # Joint indices as found by p.getJointInfo()
        self.wheel_joints = [23, 24]
        self.robot_joints = [7, 8, 9, 10, 11, 12, 13]
        #Get Number of the links
        """
        for joint_number in range(p.getNumJoints(self.robot)):
            info = p.getJointInfo(self.robot, joint_number)
            print(info[0], ": ", info[1])
        """
        self.ee_link = [15]
        self._friction = 1.0
        self.disableVelocityControl()

    def disableVelocityControl(self):
        for i in range(len(self.robot_joints)):
            p.setJointMotorControl2(
                self.robot,
                jointIndex=self.robot_joints[i],
                controlMode=p.VELOCITY_CONTROL,
                force=self._friction
            )

    def get_ids(self):
        return self.robot

    def apply_action(self, action):
        right_wheel_vel = action[0]
        left_wheel_vel = action[1]

        # Set the wheel joint velocities
        p.setJointMotorControl2(self.robot, self.wheel_joints[0],
                                    controlMode=p.VELOCITY_CONTROL,
                                    targetVelocity=right_wheel_vel)
        p.setJointMotorControl2(self.robot, self.wheel_joints[1],
                                    controlMode=p.VELOCITY_CONTROL,
                                    targetVelocity=left_wheel_vel)
        # Set joint torques
        for i in range(7):
            filteredAction = action[2+i]
            p.setJointMotorControl2(self.robot, self.robot_joints[i],
                                        controlMode=p.TORQUE_CONTROL,
                                        force=filteredAction)

    def get_observation(self):
        # Get the position and orientation of the car in the simulation
        pos, quat, _, _, _, _ = p.getLinkState(self.robot, linkIndex=15)
        rotMat = p.getMatrixFromQuaternion(quat)
        # Get the velocity of the car
        vel = p.getBaseVelocity(self.robot)[0][0:2]


        # Get Joint Configurations
        joint_pos_list = []
        joint_vel_list = []
        for i in range(7):
            pos, vel, _, torque = p.getJointState(self.robot, self.robot_joints[i])
            joint_pos_list.append(pos)
            joint_vel_list.append(vel)
        joint_pos = tuple(joint_pos_list)
        joint_vel = tuple(joint_vel_list)
        # Get Base Configuration
        posBase, quatBase = p.getBasePositionAndOrientation(self.robot)
        #eulerBase = p.getEulerFromQuaternion(quatBase)
        linVelBase, rotVelBase = p.getBaseVelocity(self.robot)
        velBase = (linVelBase[0], linVelBase[1], rotVelBase[2])
        # Get EE Pose
        posEE, quatEE, _, _, _, _ = p.getLinkState(self.robot, linkIndex=15)

        # Concatenate position, orientation, velocity
        self.observation = (joint_pos+ joint_vel + posBase[0:2] + velBase +  posEE)

        return self.observation









