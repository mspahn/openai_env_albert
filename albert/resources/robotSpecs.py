import pybullet as p
import os
import math
import numpy as np
import gym


class RobotSpecs:
    def __init__(self):
        """ Values for 4th and 6th joint modified by pi/2 compared to the website
            due to the changed initial configuration
        """
        q_lim_franka_up = np.array(
            [2.8973, 1.7628, 2.8973, 1.500996, 2.8973, 2.1817, 2.8973]
        )
        q_lim_franka_low = np.array(
            [-2.8973, -1.7628, -2.8973, -1.50100, -2.8973, -1.58830, -2.8973]
        )
        q_dot_lim_franka_low = np.array(
            [-2.1750, -2.1750, -2.1750, -2.1750, -2.6100, -2.6100, -2.6100]
        )
        q_dot_lim_franka_up = np.array(
            [+2.1750, +2.1750, +2.1750, +2.1750, +2.6100, +2.6100, +2.6100]
        )
        t_lim_franka_low = np.array([-87.0, -87.0, -87.0, -87.0, -12.0, -12.0, -12.0])
        t_lim_franka_up = np.array([+87.0, +87.0, +87.0, +87.0, +12.0, +12.0, +12.0])
        base_lim_low = np.array([-10.0, -10.0])
        base_lim_up = np.array([10.0, 10.0])
        base_vel_low = np.array([-0.5, -0.5, -0.5])
        base_vel_up = np.array([0.5, 0.5, 0.5])
        wheels_vel_low = np.array([-10.0, -10.0])
        wheels_vel_up = np.array([10.0, 10.0])
        ee_lim_low = np.array([-10.0, -10.0, 0.0])
        ee_lim_up = np.array([+10.0, +10.0, 2.5])
        self.specDict = {
            "base_lim_low": base_lim_low,
            "base_lim_up": base_lim_up,
            "base_vel_low": base_vel_low,
            "base_vel_up": base_vel_up,
            "wheels_vel_low": wheels_vel_low,
            "wheels_vel_up": wheels_vel_up,
            "q_lim_franka_low": q_lim_franka_low,
            "q_lim_franka_up": q_lim_franka_up,
            "q_dot_lim_franka_low": q_dot_lim_franka_low,
            "q_dot_lim_franka_up": q_dot_lim_franka_up,
            "t_lim_franka_low": t_lim_franka_low,
            "t_lim_franka_up": t_lim_franka_up,
            "ee_lim_low": ee_lim_low,
            "ee_lim_up": ee_lim_up,
        }

    def getActionSpace(self):
        self.low_action = np.concatenate(
            (self.specDict["wheels_vel_low"], self.specDict["t_lim_franka_low"])
        )
        self.high_action = np.concatenate(
            (self.specDict["wheels_vel_up"], self.specDict["t_lim_franka_up"])
        )
        return gym.spaces.box.Box(low=self.low_action, high=self.high_action)

    def getDiscreteActionSpace(self):
        self._discreteMapping = {0 : [0, 0, 0, -30, 0, -10, 0, 0, 0],
            1 : [0, 0, 0, -30, 0, 10, 0, 0, 0],
            2 : [0, 0, 0, 30, 0, -10, 0, 0, 0],
            3 : [0, 0, 0, 30, 0, 10, 0, 0, 0]}
        return gym.spaces.discrete.Discrete(4)

    def decodeAction(self, action):
        return self._discreteMapping[action]

    def getObservationSpace(self):
        self.low_obs = np.concatenate(
            (
                self.specDict["q_lim_franka_low"],
                self.specDict["q_dot_lim_franka_low"],
                self.specDict["base_lim_low"],
                self.specDict["base_vel_low"],
                self.specDict["ee_lim_low"],
                self.specDict["ee_lim_low"],
                self.specDict["ee_lim_low"],
            )
        )
        self.high_obs = np.concatenate(
            (
                self.specDict["q_lim_franka_up"],
                self.specDict["q_dot_lim_franka_up"],
                self.specDict["base_lim_up"],
                self.specDict["base_vel_up"],
                self.specDict["ee_lim_up"],
                self.specDict["ee_lim_up"],
                self.specDict["ee_lim_up"],
            )
        )
        return gym.spaces.box.Box(low=self.low_obs, high=self.high_obs)

    def validateObservation(self, ob):
        indices = []
        reward = 0.0
        rangeI = 7
        for i in range(rangeI):
            if not ((ob[i] <= self.high_obs[i]) and (ob[i] >= self.low_obs[i])):
                indices.append(i)
        done = (len(indices) > 0)
        if (done):
            info = "Exceeded Joint Limits, first joint was : " + str(indices[0])
            reward = -100.0
        else:
            info = ""
        return (reward, done, info)

